#Abre o arquivo contendo os nomes e retorna uma lista encadeada contendo caracteres em minusculo.
def Abrir_Arquivo(arq):
    abrir = open(arq, 'r')
    arquivo = abrir.read().lower().split('\n')
    abrir.close()
    return arquivo

def Escreve(arquivo,conteudo):
    arq = open(arquivo,'a')
    arq.write(conteudo)
    arq.close()

def NomeAbnt(string):
    x = string.split(' ')
    abrev = []
    if Sobrenome(x[-1]):
        if not Sobrenome(x[1]): #Para lidar com o sobrenome depois do nome
            abrev = x[len(x)-1].upper()+" "+x[1].upper() + ', '
            del x[1]
        elif conects(x[len(x)-2]):
            abrev = x[len(x)-2].upper() + " " + x[len(x)-1].upper() +', '
        else:
            abrev = x[len(x)-1].upper() +', '
        for i in x:
            if (conects(i) and i != x[-1] and i != x[-2] ):
                abrev += i[0].upper()+"."
            if(conects(i)==False):
                abrev+=" " + i.lower()

    else:
        if not Sobrenome(x[1]): #Para lidar com o sobrenome depois do nome
            abrev = x[len(x)-1].upper()+" "+x[1].upper() + ', '
            del x[1]
        else:
            abrev = x[len(x)-2].upper()+" "+x[len(x)-1].upper() + ', '
        for i in x:
            if (conects(i) and i!=x[-1] and i!=x[-2]):
                abrev+=i[0].upper()+"."
            if(conects(i)==False):
                abrev+=" " + i.lower()

    return abrev
    
def conects(string):
    var = ['da','do','de','das','dos','e']
    for i in var:
        if string == i:return False
    return True #
    
def Sobrenome(string):
    var = ['neto','sobrinho','junior','filho']
    for i in var:
        if string == i: return False
    return True

#Main
converter = 'Nomes.txt'
for x in Abrir_Arquivo(converter):
    Escreve('Convertidos.txt', NomeAbnt(x) + "\n")
print("Convertido com Sucesso!")